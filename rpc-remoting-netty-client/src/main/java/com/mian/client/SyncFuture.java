//package com.mian.client;
//
//import com.mian.bean.RpcResponse;
//
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.CountDownLatch;
//import java.util.concurrent.Future;
//import java.util.concurrent.TimeUnit;
//
//public class SyncFuture implements Future {
//
//    public final static ConcurrentHashMap<String,SyncFuture> FUTURE_MAP = new ConcurrentHashMap<>();
//
//    // 因为请求和响应是一一对应的，因此初始化CountDownLatch值为1。
//    private CountDownLatch latch = new CountDownLatch(1);
//    // 需要响应线程设置的响应结果
//    private RpcResponse response;
//    // Futrue的请求时间，用于计算Future是否超时
//    private long beginTime = System.currentTimeMillis();
//
//    public SyncFuture(String requestId) {
//        FUTURE_MAP.put(requestId, this);
//    }
//    @Override
//    public boolean cancel(boolean mayInterruptIfRunning) {
//        return false;
//    }
//    @Override
//    public boolean isCancelled() {
//        return false;
//    }
//    @Override
//    public boolean isDone() {
//        if (response != null) {
//            return true;
//        }
//        return false;
//    }
//
//    // 获取响应结果，直到有结果才返回。
//    @Override
//    public RpcResponse get() throws InterruptedException {
//        latch.await();
//        return this.response;
//    }
//
//    // 获取响应结果，直到有结果或者超过指定时间就返回。
//    @Override
//    public RpcResponse get(long timeout, TimeUnit unit) throws InterruptedException {
//        if (latch.await(timeout, unit)) {
//            return this.response;
//        }
//        return null;
//    }
//
//    // 用于设置响应结果，并且做countDown操作，通知请求线程
//    public void setResponse(RpcResponse response) {
//        this.response = response;
//        latch.countDown();
//    }
//
//    public long getBeginTime() {
//        return beginTime;
//    }
//
//    // channelRead中设置结果
//    public static void receive(RpcResponse response){
//        SyncFuture df = FUTURE_MAP.get(response.getRequestId());
//        if (df != null){
//            df.setResponse(response);
//            FUTURE_MAP.remove(response.getRequestId());
//        }
//    }
//
//    // 定期清理 全局map 略
//}