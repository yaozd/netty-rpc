package com.mian.netty.demo;


import com.mian.annotation.Reference;
import com.mian.netty.demo.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:application.xml"})
public class ConsumerTest {

    @Reference
    DemoService demoService;

    /**
     * 返回基本类型
     * 测试结果 -- OK
     */
    @Test
    public void testString(){

        String result = demoService.sayHello("mianhuajun");
        System.out.println(result);
    }

    /**
     * 返回基本类型
     * 测试结果 -- OK
     */
    @Test
    public void testInt(){
        System.out.println(demoService.getUserAge("lili"));
    }

    /**
     * 返回对象
     * 测试结果 -- OK
     */
    @Test
    public void testObj(){
        User user = demoService.getUser("lili");
        System.out.println(user);
    }

    /**
     * 服务器端设置 执行时间为 700ms
     * 客户端 长链接(无超时断开)
     * 测试结果 -- OK
     */
    @Test
    public void test2() throws InterruptedException {

        // 服务器端设置的执行等待时间不变，客户端改为长连接(无超时断开)
        for(int i=0; i<10; i++){
            String request = "mianhua_"+i;
            String result = demoService.sayHello(request);
            System.out.println(request+"===="+result);
        }

    }


    /**
     * 服务器端 执行时间设置为 700ms
     * 使用长链((5秒没有通信关闭) 测试结果发发现
     * 测试结果 -- 执行到i=5的时候，服务端执行完毕，客户端 channelRead 读取不到结果，导致结果阻塞
     */
    @Test
    public void test() throws InterruptedException {

        // 使用长链(5秒没有通信则关闭)
        // 1.服务器端执行较慢的情况下，会不会有channel关闭了，响应还没有传回来
            // 服务器端设置执行时间500ms 客户端设置5没有通信则关闭，发现执行到i=9的时候，服务器端执行万了，客户端始终等待
        for(int i=0; i<10; i++){
            String request = "mianhua_"+i;
            String result = demoService.sayHello(request);
            System.out.println(request+"===="+result);
        }

    }




}
